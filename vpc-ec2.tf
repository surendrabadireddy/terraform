provider "aws" {
  region     = "us-east-2"
  access_key = "AKIAZAAMWYAZA4U64XEM"
  secret_key = "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"
}

resource "aws_vpc" "task" {
  cidr_block       = "192.168.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "task-vpc"
  }
}

resource "aws_subnet" "pub" {
  vpc_id     = aws_vpc.task.id
  cidr_block = "192.168.1.0/24"
  tags = {
    Name = "public"
  }
}

resource "aws_subnet" "pri" {
  vpc_id     = aws_vpc.task.id
  cidr_block = "192.168.3.0/24"
  map_public_ip_on_launch = true
  tags = {
    Name = "private"
  }
}

resource "aws_internet_gateway" "igw1" {
  vpc_id = aws_vpc.task.id

  tags = {
    Name = "IGW"
  }
}

resource "aws_eip" "ip" {
  vpc      = true
}

resource "aws_nat_gateway" "ngw1" {
  allocation_id = aws_eip.ip.id
  subnet_id     = aws_subnet.pri.id

  tags = {
    Name = "NGW"
  }
}



resource "aws_route_table" "rt1" {
  vpc_id = aws_vpc.task.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw1.id
  }
  tags = {
    Name = "custom"
  }
}

resource "aws_route_table" "rt2" {
  vpc_id = aws_vpc.task.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.ngw1.id
  }
  tags = {
    Name = "main"
  }
}

resource "aws_route_table_association" "ass_1" {
  subnet_id      = aws_subnet.pub.id
  route_table_id = aws_route_table.rt1.id
}

resource "aws_route_table_association" "ass_2" {
  subnet_id      = aws_subnet.pri.id
  route_table_id = aws_route_table.rt2.id
}



resource "aws_security_group" "sg" {
  name        = "first_sg"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.task.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.task.cidr_block]

  }
  ingress {
    description      = "TLS from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }  

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "first_sg"
  }
}
resource "aws_instance" "web" {
  ami           = "ami-064ff912f78e3e561"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.pub.id
  key_name = aws_key_pair.key.id
  user_data= <<-EOF
             #!/bin/bash
             sudo yum install httpd -y
             sudo systemctl start httpd
             EOF

  tags = {
    Name = "suri-terraform"
  }
}
resource "aws_key_pair" "key" {
  key_name   = "sample"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDS3B54PEQOJnbCyryOIPKPdhnx7fhQGJA51/Jr/0IAzriibqYUsR0UmQiRPVhOIsCeyjsPO5Av1asTlBzd6lBJvoMEYeTUNbW4goxk7yPs59g9I5dnm5pZrMlg+pz7KpisVHOUJ7ws4GiY7eVl6hrEW1EbFHtWeRtliUzDCWJSpaUFclUJ8oSH4vUbT9MslpognyyjT/KX+R5pDRZWskISP4sGoPie3SD0BEThaOh2aFvBOhGil2ZfbKzjUx1YhLZC+HV5dS4G4TBCTVFW72I+baEwtCqjWbFaxUXoN/Az/gUFXaHDn27iLcM5B5zXm1OxNR3RqUJRE2ECfPDqRydp root@ip-172-31-18-147.us-east-2.compute.internal"
}
